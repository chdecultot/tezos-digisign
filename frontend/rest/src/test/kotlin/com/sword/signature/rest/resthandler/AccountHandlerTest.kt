package com.sword.signature.rest.resthandler

import com.sword.signature.api.account.AccountCreate
import com.sword.signature.business.exception.HoneyPotFieldNotEmptyException
import com.sword.signature.business.model.AccountCreate as BusinessAccountCreate
import com.sword.signature.business.exception.PasswordTooWeakException
import com.sword.signature.business.model.Account
import com.sword.signature.business.service.AccountService
import com.sword.signature.business.service.MailService
import com.sword.signature.rest.authentication.checkPassword
import com.sword.signature.rest.configuration.OptionalFeaturesConfig
import com.sword.signature.webcore.authentication.JwtTokenService
import io.mockk.*
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

internal class AccountHandlerTest(
) {

    private val accountService: AccountService = mockk()
    private val bCryptPasswordEncoder: BCryptPasswordEncoder = mockk()
    private val mailService: MailService = mockk()
    private val jwtTokenService: JwtTokenService = mockk()
    private val optionalFeaturesConfig: OptionalFeaturesConfig = mockk()

    private val accountHandler = AccountHandler(
        accountService,
        bCryptPasswordEncoder,
        mailService,
        jwtTokenService,
        optionalFeaturesConfig
    )

    @BeforeEach
    fun resetMocks() {
        clearMocks(accountService, bCryptPasswordEncoder, mailService, jwtTokenService, optionalFeaturesConfig)
    }

    @Test
    fun `register uses correct parameters`() {

        val expectedHash = "defaultHash"
        val expectedPublicKey = "defaultPublicKey"
        val expectedSignatureLimit = 5
        val expectedIsAdmin = false

        val login = "login"
        val email = "email"
        val fullName = "fullName"
        val company = "company"
        val country = "country"

        // Mocking things we don't test
        every { bCryptPasswordEncoder.encode(any()) } returns ""
        every { jwtTokenService.generateVolatileToken(any(), any(), any()) } returns ""
        every { mailService.sendEmail(any()) } returns Unit

        // Mocking the config
        every { optionalFeaturesConfig.registering.defaultPublicKey } returns expectedPublicKey
        every { optionalFeaturesConfig.registering.defaultHash } returns expectedHash
        every { optionalFeaturesConfig.registering.defaultSignatureLimit } returns expectedSignatureLimit

        // Prepare the capture of the argument passed to the service by the handler
        val argument = slot<BusinessAccountCreate>()
        coEvery {
            accountService.createAccount(any(), capture(argument))
        } returns Account(
            id = "", login = "", email = "", fullName = "", company = "", password = "", country = "", publicKey = "",
            hash = "", isAdmin = true, signatureLimit = null, disabled = false, firstLogin = false
        ) // Mocks createAccount, not tested

        val registeringAccount = AccountCreate(
            login = login,
            email = email,
            fullName = fullName,
            company = company,
            country = country,
            publicKey = "",
            hash = "",
            isAdmin = true,
            signatureLimit = null
        )

        runBlocking {
            accountHandler.register("", registeringAccount) // Tested endpoint

            val accountCaptured = argument.captured // Captured at the call of createAccount()

            assertEquals(login, accountCaptured.login)
            assertEquals(email, accountCaptured.email)
            assertEquals(fullName, accountCaptured.fullName)
            assertEquals(company, accountCaptured.company)
            assertEquals(country, accountCaptured.country)

            assertEquals(expectedPublicKey, accountCaptured.publicKey)
            assertEquals(expectedHash, accountCaptured.hash)
            assertEquals(expectedIsAdmin, accountCaptured.isAdmin)
            assertEquals(expectedSignatureLimit, accountCaptured.signatureLimit)
        }

    }

    @Test
    fun `Can't register if honeypot fields are filled`() {
        val botRegistration1 = AccountCreate(
            login = "login",
            email = "email",
            fullName = "fullName",
            company = "company",
            country = "country",
            publicKey = "",
            hash = "",
            isAdmin = true,
            signatureLimit = null,
            honeyPotField1 = "Some spam text"
        )

        val botRegistration2 = AccountCreate(
            login = "login",
            email = "email",
            fullName = "fullName",
            company = "company",
            country = "country",
            publicKey = "",
            hash = "",
            isAdmin = true,
            signatureLimit = null,
            honeyPotField2 = "Some other spam text"
        )

        val botRegistration3 = AccountCreate(
            login = "login",
            email = "email",
            fullName = "fullName",
            company = "company",
            country = "country",
            publicKey = "",
            hash = "",
            isAdmin = true,
            signatureLimit = null,
            honeyPotField1 = "did you know spam is also brand of canned pork ?",
            honeyPotField2 = "spamming"
        )

        assertThrows<HoneyPotFieldNotEmptyException> {
            runBlocking { accountHandler.register("", botRegistration1) }
        }
        assertThrows<HoneyPotFieldNotEmptyException> {
            runBlocking { accountHandler.register("", botRegistration2) }
        }
        assertThrows<HoneyPotFieldNotEmptyException> {
            runBlocking { accountHandler.register("", botRegistration3) }
        }
    }

    @Test
    fun `password too short`() {
        assertThrows<PasswordTooWeakException> {
            checkPassword("S+tr0ng")
        }
    }

    @Test
    fun `password too weak`() {
        assertThrows<PasswordTooWeakException> {
            checkPassword("tooweak01")
        }
    }

    @Test
    fun `password strong`() {
        checkPassword("S+tr0ng!")
    }
}
