package com.sword.signature.rest.resthandler

import com.sword.signature.api.account.Account
import com.sword.signature.api.auth.AuthRequest
import com.sword.signature.api.auth.AuthResponse
import com.sword.signature.api.auth.ForgotPwdRequest
import com.sword.signature.api.auth.ForgotPwdResponse
import com.sword.signature.business.model.mail.ResetPasswordMail
import com.sword.signature.business.service.AccountService
import com.sword.signature.business.service.MailService
import com.sword.signature.webcore.authentication.CustomUserDetails
import com.sword.signature.webcore.authentication.JwtTokenService
import com.sword.signature.webcore.mapper.toWeb
import io.swagger.v3.oas.annotations.Parameter
import org.springframework.beans.factory.annotation.Value
import org.springframework.security.authentication.BadCredentialsException
import org.springframework.security.core.annotation.AuthenticationPrincipal
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.bind.annotation.*
import java.time.Duration

@RestController
@RequestMapping("\${api.base-path:/api}")
class AuthHandler(
    private val accountService: AccountService,
    private val bCryptPasswordEncoder: BCryptPasswordEncoder,
    private val jwtTokenService: JwtTokenService,
    val mailService: MailService,
    @Value("\${jwt.duration}") private val tokenDuration: Duration
) {

    @RequestMapping(
        value = ["/auth"],
        consumes = ["application/json"],
        produces = ["application/json"],
        method = [RequestMethod.POST]
    )
    suspend fun auth(
        @Parameter(
            description = "request of autentification",
            required = true
        ) @RequestBody request: AuthRequest
    ): AuthResponse {
        val user = accountService.getAccountByLoginOrEmail(request.user)
            ?: throw BadCredentialsException("Invalid credentials")

        if (!bCryptPasswordEncoder.matches(request.password, user.password)) {
            throw BadCredentialsException("Invalid credentials")
        }

        val token = jwtTokenService.generateVolatileToken(user.id, tokenDuration)

        return AuthResponse(token = token)
    }

    @RequestMapping(
        value = ["/me"],
        produces = ["application/json"],
        method = [RequestMethod.GET]
    )
    suspend fun me(
        @AuthenticationPrincipal user: CustomUserDetails
    ): Account {
        return user.account.toWeb()
    }

    @RequestMapping(
        value = ["/forgotten-password"],
        consumes = ["application/json"],
        produces = ["application/json"],
        method = [RequestMethod.POST]
    )
    suspend fun forgotPassword(
        @Parameter(
            description = "request to send an email to reset the user's password",
            required = true
        )
        @RequestHeader("origin") origin: String,
        @RequestBody request: ForgotPwdRequest
    ): ForgotPwdResponse {
        val user = accountService.getAccountByLoginOrEmail(request.usernameOrEmail)
            ?: throw BadCredentialsException("Invalid credentials")

        val token = jwtTokenService.generateVolatileToken(user.id, Duration.ofDays(1), user.password)

        mailService.sendEmail(ResetPasswordMail(recipient = user, link = "$origin/#/reset-password/$token"))

        return ForgotPwdResponse(maskedEmail = maskEmail(user.email))
    }

    private fun maskEmail(email: String): String {
        val parts: List<String> = email.split("@")
        return parts[0][0] + "****@" + parts[1]
    }

}
