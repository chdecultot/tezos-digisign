package com.sword.signature.rest

import com.sword.signature.api.custom.OptionalFeatures
import com.sword.signature.rest.configuration.OptionalFeaturesConfig

fun OptionalFeaturesConfig.toWeb(): OptionalFeatures {
    return OptionalFeatures(registering = OptionalFeatures.Registering(enabled = registering.enabled))
}
